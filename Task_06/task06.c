#include <linux/errno.h>
#include <linux/module.h>
#include <linux/miscdevice.h>  // for register and deregister
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>

MODULE_LICENSE("Dual BSD/GPL");

#define USER_ID "06695cc671ef"
#define USER_ID_LEN 12


static ssize_t eudy_read(struct file *filep, char __user *buff,
			    size_t count, loff_t *offp)
{
	return simple_read_from_buffer(buff, count, offp, USER_ID,
				      USER_ID_LEN);
}

static ssize_t eudy_write(struct file *filep, const char *buff,
			    size_t count, loff_t *offp)
{
	ssize_t retval;
	char *stuff = kmalloc(sizeof(char) * (USER_ID_LEN + 1), GFP_KERNEL);

	if (stuff == NULL)
		return -ENOMEM;

	if (count == USER_ID_LEN) {
		retval = simple_write_to_buffer(stuff, USER_ID_LEN,
						offp, buff, count);
		stuff[USER_ID_LEN] = '\0';
	} else {
		return -EINVAL;
	}

	if ((*offp) == strlen(USER_ID) + 1) {
		retval = strncmp(stuff, USER_ID, USER_ID_LEN) ?
			-EINVAL : retval;
	}

	kfree(stuff);
	return retval;
}


static const struct file_operations hello_fops = {
	.owner			= THIS_MODULE,
	.write			= eudy_write,
	.read			= eudy_read,
	.llseek                 = no_llseek,
};


static struct miscdevice hello_dev;

int hello_init(void)
{
	int retval;
	pr_alert("Hello Evil World..");
	hello_dev.minor = MISC_DYNAMIC_MINOR;
	hello_dev.name = "eudyptula";
	hello_dev.fops = &hello_fops;
	retval = misc_register(&hello_dev);
	if (retval)
		return retval;
	return 0;
}

void hello_exit(void)
{
	misc_deregister(&hello_dev);
	pr_alert("Bye Evil World..!!");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_AUTHOR("Pratik Jain <pratik.jain448@gmail.com>");
