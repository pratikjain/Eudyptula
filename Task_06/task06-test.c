#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define CDEV_DEVICE "eudyptula"
#define USER_ID "06695cc671ef"
#define USER_ID_LEN 12

static char buf[512+1];

int main(int argc, char *argv[])
{
	int fd, len;

	if ((fd = open("/dev/" CDEV_DEVICE, O_RDWR)) < 0) {
		perror("/dev/" CDEV_DEVICE);
		exit(1);
	}

	printf("fd :%d\n",fd);
	len = strlen(USER_ID);
	if (read(fd, buf, len) < 0)
		perror("read()");
	else
		printf("Before: \"%s\".\n", buf);
	char buf1[] = USER_ID;
	char buf2[] = USER_ID "23";
	if (write(fd, buf1, strlen(buf1)) < 0)
		perror("write()");
	else
		printf("Wrote: \"%s\".\n", buf1);

	if (write(fd, buf2, strlen(buf2)) < 0)
		perror("write()");
	else
		printf("Wrote: \"%s\".\n", buf2);
	if ((close(fd)) < 0) {
		perror("close()");
		exit(1);
	}
	exit(0);
}
