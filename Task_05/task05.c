#include <linux/init.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <uapi/linux/hid.h>

// Give licensing otherwise Kernel Complains
// HID - human interface device

MODULE_LICENSE("Dual BSD/GPL");


/**
 * USB_INTERFACE_INFO - macro used to describe a class of usb
 * interfaces
 * @cl: bInterfaceClass value
 * @sc: bInterfaceSubClass value
 * @pr: bInterfaceProtocol value
 *
 * This macro is used to create a struct usb_device_id that matches a
 * @id_table: USB drivers use ID table to support hotplugging.
 *	Export this with MODULE_DEVICE_TABLE(usb,...).  This must be set
 *	or your driver's probe function will never get called.
 * specific class of interfaces.

 *From usb.h
 *
 */

static struct usb_device_id my_table[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
			     USB_INTERFACE_SUBCLASS_BOOT,
			     USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
			     USB_INTERFACE_SUBCLASS_BOOT,
			     USB_INTERFACE_PROTOCOL_MOUSE) },
	{}
};

MODULE_DEVICE_TABLE(usb, my_table);

static int hello_init(void)
{
	pr_alert("Hello evil world..!!\n");
	return 0;
}

static void hello_exit(void)
{
	pr_alert("Bye evil world..!!\n");
}

module_init(hello_init);
module_exit(hello_exit);
