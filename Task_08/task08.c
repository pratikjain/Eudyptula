#include <linux/errno.h>
#include <linux/module.h>
#include <linux/miscdevice.h>  // for register and deregister
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/debugfs.h>
#include <linux/jiffies.h>
#include <linux/spinlock.h>


MODULE_LICENSE("Dual BSD/GPL");


#define USER_ID "06695cc671ef"
#define USER_ID_LEN 12
#define DIR_NAME "eudyptula"
#define RW_FILE_NAME "06695cc671ef"
#define JIF_FILE_NAME "jiffies"
#define PAGE_FILE_NAME "foo"
#define MAX_FOO_BUFF_SIZE 1024

rwlock_t lock = __RW_LOCK_UNLOCKED(lock);
unsigned long flags;
static char *foo_buff;
static int foo_buff_len;

static int rw_open(struct inode *inode, struct file *filp)
{
	filp->private_data = inode->i_private;
	return 0;
}

static ssize_t rw_read(struct file *filep, char __user *buff,
			 size_t count, loff_t *offp)
{
	return simple_read_from_buffer(buff, count, offp, USER_ID,
				       USER_ID_LEN);
}

static ssize_t rw_write(struct file *filep, const char *buff,
			  size_t count, loff_t *offp)
{
	ssize_t retval;
	char *stuff = kmalloc(sizeof(char) * (USER_ID_LEN + 1), GFP_KERNEL);

	if (stuff == NULL)
		return -ENOMEM;

	if (count == USER_ID_LEN) {
		retval = simple_write_to_buffer(stuff, USER_ID_LEN,
						offp, buff, count);
		stuff[USER_ID_LEN] = '\0';
	} else {
		return -EINVAL;
	}

	if ((*offp) == strlen(USER_ID) + 1) {
		retval = strncmp(stuff, USER_ID, USER_ID_LEN) ?
			-EINVAL : retval;
	}

	kfree(stuff);
	return retval;
}

static ssize_t page_read(struct file *filep, char __user *buff,
			 size_t count, loff_t *offp)
{
	ssize_t retval;

	read_lock_irqsave(&lock, flags);
	retval = copy_to_user(buff, foo_buff, foo_buff_len);
	if (retval != 0)
		return -EFAULT;
	read_unlock_irqrestore(&lock, flags);
	return retval;
}

static ssize_t page_write(struct file *filep, const char __user *buff,
			  size_t count, loff_t *offp)
{
	ssize_t retval;

	write_lock_irqsave(&lock, flags);
	if (*offp > MAX_FOO_BUFF_SIZE)
		return -EINVAL;
	retval = copy_from_user(foo_buff, buff, count);
	if (retval != 0) {
		foo_buff_len = 0;
		return -EFAULT;
	}
	foo_buff_len = count;
	foo_buff[count] = '\0';
	write_unlock_irqrestore(&lock, flags);
	return retval;
}

static const struct file_operations rw_fops = {
	.owner			= THIS_MODULE,
	.write			= rw_write,
	.read			= rw_read,
	.llseek                 = no_llseek,
	.open                   = rw_open,
};

static const struct file_operations page_fops = {
	.owner			= THIS_MODULE,
	.write			= page_write,
	.read			= page_read,
	.llseek                 = no_llseek,
};

struct dentry *directory;
static u64 jtym;

static int hello_init(void)
{
	int fileval;
	int pageval;
	struct dentry *dfile_rw;
	struct dentry *dfile_jif;
	struct dentry *dfile_page;

	jtym = jiffies;
	foo_buff_len = USER_ID_LEN + 1;
	foo_buff = kmalloc(sizeof(char) * (MAX_FOO_BUFF_SIZE + 1), GFP_KERNEL);
	if (foo_buff == NULL)
		return -ENOMEM;

	pr_alert("Hello Evil World..");

	directory = debugfs_create_dir(DIR_NAME, NULL);
	// Read-Write by any user
	dfile_rw = debugfs_create_file(RW_FILE_NAME, 0666, directory,
				    &fileval, &rw_fops);
	// Read Only by any user
	dfile_jif = debugfs_create_u64(JIF_FILE_NAME, 0444, directory, &jtym);
	// Write by root and Read-only by others
	dfile_page = debugfs_create_file(PAGE_FILE_NAME, 0644, directory,
					 &pageval, &page_fops);
	if (!dfile_rw || !dfile_jif || !dfile_page)
		return -ENOENT;
	return 0;
}

static void hello_exit(void)
{
	debugfs_remove_recursive(directory);
	kfree(foo_buff);
	pr_alert("Bye Evil World..!!");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_AUTHOR("Pratik Jain <pratik.jain448@gmail.com>");
