#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define USER_ID "06695cc671ef"
#define CDEV_DEVICE "eudyptula"
#define DEBUGFS_ID_FILE USER_ID
#define DEBUGFS_PAGE_FILE "foo"
#define DEBUGFS_JIFF_FILE "jiffies"
#define USER_ID_LEN 12

static char buf[512+1];

int main(int argc, char *argv[])
{
	int fd, len;
	fprintf(stderr, "Testing id\n");
	if ((fd = open("/sys/kernel/debug/eudyptula/" DEBUGFS_ID_FILE, O_RDWR)) < 0) {
		perror("/sys/kernel/debug/eudyptula/" DEBUGFS_ID_FILE);
		exit(1);
	}

	printf("fd :%d\n",fd);
	len = strlen(USER_ID);
	if (read(fd, buf, len) < 0)
		perror("read()");
	else
		printf("Before: \"%s\".\n", buf);
	char buf1[] = USER_ID;
	char buf2[] = USER_ID "23";
	if (write(fd, buf1, strlen(buf1)) < 0)
		perror("write()");
	else
		printf("Wrote: \"%s\".\n", buf1);

	if (write(fd, buf2, strlen(buf2)) < 0)
		perror("write()");
	else
		printf("Wrote: \"%s\".\n", buf2);
	if ((close(fd)) < 0) {
		perror("close()");
		exit(1);
	}

	fprintf(stderr, "Testing foo\n");
	if ((fd = open("/sys/kernel/debug/eudyptula/" DEBUGFS_PAGE_FILE, O_RDWR)) < 0) {
		perror("/sys/kernel/debug/eudyptula/" DEBUGFS_PAGE_FILE);
		exit(1);
	}

	printf("fd :%d\n",fd);
	len = strlen(USER_ID);
	if (read(fd, buf, len) < 0)
		perror("read()");
	else
		printf("Before: \"%s\".\n", buf);
	if (write(fd, buf1, strlen(buf1)) < 0)
		perror("write()");
	else
		printf("Wrote: \"%s\".\n", buf1);

	if (write(fd, buf2, strlen(buf2)) < 0)
		perror("write()");
	else
		printf("Wrote: \"%s\".\n", buf2);
	sleep(2);
	if (read(fd, buf, len) < 0)
		perror("read()");
	else
		printf("Before: \"%s\".\n", buf);
	if ((close(fd)) < 0) {
		perror("close()");
		exit(1);
	}
	FILE *fp = fopen("/sys/kernel/debug/eudyptula/" DEBUGFS_JIFF_FILE, "r");
	unsigned long long id;
	fscanf(fp, "%llu", &id);
	printf("Read from jiffies: %llu\n", id);
	exit(0);
}
